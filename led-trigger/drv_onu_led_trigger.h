/*
 *  ONU LED trigger driver
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2  as published
 *  by the Free Software Foundation.
 *
 */
#ifndef _INCLUDE_ONU_LED_TRIGGER_H_
#define _INCLUDE_ONU_LED_TRIGGER_H_

#define ONU_LED_TRIGGER_NAME         "onu_led_trigger"
#define ONU_LED_TRIGGER_DESC         "FALC(tm) ON LED trigger"
#define ONU_LED_TRIGGER_VERSION      "1.0.1"

#endif	/* _INCLUDE_ONU_LED_TRIGGER_H_ */
